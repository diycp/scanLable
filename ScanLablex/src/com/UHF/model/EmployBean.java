package com.UHF.model;

import java.util.Date;

public class EmployBean {
	private String id;// 公司类型
	
	private String companyType;// 公司类型

	/**
	 * 车间主键
	 */
	private String workShopId;

	/**
	 * 编号
	 */
	private String userNo;

	/**
	 * 姓名
	 */
	private String userName;

	/**
	 * 密码
	 */
	private String password;

	/**
	 * 类型
	 */
	private String userType;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public String getWorkShopId() {
		return workShopId;
	}

	public void setWorkShopId(String workShopId) {
		this.workShopId = workShopId;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}


}