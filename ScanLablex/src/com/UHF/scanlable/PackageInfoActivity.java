package com.UHF.scanlable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityGroup;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.UHF.R;
import com.UHF.model.AcceptanceAdapter;
import com.UHF.model.CheckBoxAdapter;
import com.UHF.util.CheckDataFromServer;

public class PackageInfoActivity extends Activity implements OnClickListener{

	Button button_write;

	EditText et_packageInventoryNum0;
	EditText et_materialCode0;
	EditText et_name0;
	EditText et_specification0;
	EditText et_producers0;
	EditText et_supplier0;
	EditText et_fromBatchNo0;
	TextView et_producers;
	TextView et_supplier;
	TextView et_fromBatchNo;
	EditText et_batchNum0;
	EditText et_count0;
	EditText et_status0;
	EditText et_position0;
	EditText et_countx0;

	
	Button button_back;
	/**
	 * 单据号/状态
	 */
	private LinkedHashMap<Object,Object> listData=new LinkedHashMap<Object,Object>();
	private CheckBoxAdapter checkAdapter;
	private Intent intent;
	private String inventoryNumber;
	private String typex;
	private String scanCode;
	private String inventoryRegularId;
	private String serialNo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.package_info);
		button_write = (Button)findViewById(R.id.button_write);
		button_write.setOnClickListener(this);
		button_back = (Button)findViewById(R.id.button_back);
		button_back.setOnClickListener(this);
		inventoryNumber = getIntent().getStringExtra("inventoryNumber");
		inventoryRegularId = getIntent().getStringExtra("inventoryRegularId");
		serialNo = getIntent().getStringExtra("serialNo");
		typex= getIntent().getStringExtra("typex");
		scanCode= getIntent().getStringExtra("scanCode");
		intent = new Intent(this,ProductIncomingActivity.class);

		et_packageInventoryNum0=(EditText)findViewById(R.id.et_packageInventoryNum0);
		et_materialCode0=(EditText)findViewById(R.id.et_materialCode0);
		et_name0=(EditText)findViewById(R.id.et_name0);
		et_specification0=(EditText)findViewById(R.id.et_specification0);
		et_producers0=(EditText)findViewById(R.id.et_producers0);
		et_supplier0=(EditText)findViewById(R.id.et_supplier0);
		et_fromBatchNo0=(EditText)findViewById(R.id.et_fromBatchNo0);
		et_batchNum0=(EditText)findViewById(R.id.et_batchNum0);
		et_count0=(EditText)findViewById(R.id.et_count0);
		et_status0=(EditText)findViewById(R.id.et_status0);
		et_position0=(EditText)findViewById(R.id.et_position0);
		et_countx0=(EditText)findViewById(R.id.et_countx0);
		et_producers=(TextView)findViewById(R.id.et_producers);
		et_supplier=(TextView)findViewById(R.id.et_supplier);
		et_fromBatchNo=(TextView)findViewById(R.id.et_fromBatchNo);

		refreshList();
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==button_write.getId()){
			submitPackage();
		}else if(v.getId()==button_back.getId()){
			onBackPressed();
		}
	}
	public boolean submitPackage(){
		CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
		String inventoryCount=et_countx0.getText().toString();
		Map m=new HashMap();
		m.put("user_id", UfhData.getEmployBean().getId());
		m.put("typex", typex);//1物料 4成品
		m.put("types", "1");//0改状态，1改数量+状态
		m.put("inventoryRegularId", inventoryRegularId);
		m.put("inventoryNumber", inventoryNumber);
		m.put("inventoryCount", inventoryCount);//实际量
		m.put("scanCode", scanCode);
		checkDataFromServer.setOperType("36");//成品入库待复核单据
		checkDataFromServer.setData(m);
		checkDataFromServer.setIp(UfhData.getIP());
		checkDataFromServer.setmHandler(handler);
		checkDataFromServer.setWhat(1);
		checkDataFromServer.checkData();
		return true;
	}
	public boolean refreshList(){
		CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
		Map m=new HashMap();
		m.put("user_id", UfhData.getEmployBean().getId());
		m.put("scanCode", scanCode);
		m.put("inventoryRegularId", inventoryRegularId);
		m.put("inventoryNumber", inventoryNumber);
		checkDataFromServer.setOperType("37");//成品入库待复核单据
		checkDataFromServer.setData(m);
		checkDataFromServer.setIp(UfhData.getIP());
		checkDataFromServer.setmHandler(handler);
		checkDataFromServer.setWhat(0);
		checkDataFromServer.checkData();
		return true;
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, InventoryPackageActivity.class); 
		intent.putExtra("typex", typex);//类型（1物料 2托盘3库位4成品）
		intent.putExtra("inventoryNumber", inventoryNumber);
		intent.putExtra("inventoryRegularId", inventoryRegularId);
		intent.putExtra("serialNo", serialNo);
        startActivity(intent);
        finish();
	}
	
	


	private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        		super.handleMessage(msg);
                switch (msg.what) {
    			case 0:
    				try {
    					String s= (String) msg.obj;
    					if(s==null||"-1".equals(s)){
    						Toast.makeText(PackageInfoActivity.this,"单据号有误!",Toast.LENGTH_SHORT).show();
    					}else{
    						JSONArray objList;
    						try {
    		    				JSONObject ss= new JSONObject(s);
    		    				String operType=ss.getString("operType");
    							objList = new JSONArray(ss.getString("list"));
    							String types="";
    							for (int i = 0; i< objList.length(); i++) {
    				                //循环遍历，依次取出JSONObject对象
    				                //用getInt和getString方法取出对应键值
    				                JSONObject obj = objList.getJSONObject(i);
    				                /*acceptanceData.put(checkHex(Long.toHexString(Long.parseLong(obj.get("epcId").toString())).toUpperCase(),"0001"),obj.get("packageInventoryNum"));//epc,流水号
    				                String backGroundColor=obj.get("backGroundColor").equals("0")?"已复核":"未复核";
    				                acceptanceCheckData.put(obj.get("packageInventoryNum").toString(),backGroundColor);//epc,流水号*/
    				                if(obj.isNull("packageInventoryNum")||!"".equals(obj.get("packageInventoryNum"))){
    				                	et_packageInventoryNum0.setText(obj.getString("packageInventoryNum"));
    				                }
    				                if(obj.isNull("materialCode")||!"".equals(obj.get("materialCode"))){
    				                	et_materialCode0.setText(obj.getString("materialCode"));
    				                }
    				                if(obj.isNull("name")||!"".equals(obj.get("name"))){
    				                	et_name0.setText(obj.getString("name"));
    				                }
    				                if(obj.isNull("specification")||!"".equals(obj.get("specification"))){
    				                	et_specification0.setText(obj.getString("specification"));
    				                }
    				                if("1".equals(typex)){
    									if(obj.isNull("producers")||!"".equals(obj.get("producers"))){
        				                	et_producers0.setText(obj.getString("producers"));
        				                }
    									if(obj.isNull("supplier")||!"".equals(obj.get("supplier"))){
        				                	et_supplier0.setText(obj.getString("supplier"));
        				                }
        				                if(obj.isNull("fromBatchNo")||!"".equals(obj.get("fromBatchNo"))){
        				                	et_fromBatchNo0.setText(obj.getString("fromBatchNo"));
        				                }
    								}else{
										et_producers0.setVisibility(View.GONE); 
										et_producers.setVisibility(View.GONE); 
										et_supplier0.setVisibility(View.GONE); 
										et_supplier.setVisibility(View.GONE); 
										et_fromBatchNo0.setVisibility(View.GONE); 
										et_fromBatchNo.setVisibility(View.GONE); 
    								}
    				                if(obj.isNull("batchNum")||!"".equals(obj.get("batchNum"))){
    				                	et_batchNum0.setText(obj.getString("batchNum"));
    				                }
    				                if(obj.isNull("position")||!"".equals(obj.get("position"))){
    				                	et_position0.setText(obj.getString("position"));
    				                }
    				                if(obj.isNull("accountCount")||!"".equals(obj.get("accountCount"))){
    				                	et_count0.setText(obj.getString("accountCount"));
    				                }
    				                if(obj.isNull("inventoryCount")||!"".equals(obj.get("inventoryCount"))){
    				                	et_countx0.setText(obj.getString("inventoryCount"));
    				                }
//    				                if(obj.isNull("taskPerson")||!"".equals(obj.get("taskPerson"))){
//    				                	et_status0.setText("未盘点");
//    				                }else{
//    				                	et_status0.setText("已盘点");
//    				                }
    				                if(obj.isNull("status")||!"".equals(obj.get("status"))){
    				                	et_status0.setText(obj.getString("status"));
    				                }else{
    				                	et_status0.setText(obj.getString("status"));
    				                }
    				             }
    						} catch (JSONException e) {
    							// TODO Auto-generated catch block
    							e.printStackTrace();
    						}
    					}
					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(PackageInfoActivity.this,"单据号有误!",Toast.LENGTH_SHORT).show();
    				}
    				break;
    			case 1:
    				try {
						String s=(String) msg.obj;
						if(s==null||"1".equals(s)){
							Toast.makeText(PackageInfoActivity.this,"盘点失败!",Toast.LENGTH_SHORT).show();
						}else{
							try {
								String re="";
								JSONObject ss= new JSONObject(s);
								String operType=ss.getString("operType");
								String result=ss.getString("result");
								if("0".equals(result)){
									Toast.makeText(PackageInfoActivity.this,"盘点成功",Toast.LENGTH_SHORT).show();
									refreshList();
								}else {
									Toast.makeText(PackageInfoActivity.this,"盘点失败",Toast.LENGTH_SHORT).show();
								}
							} catch (Exception e) {
								// TODO: handle exception
								Toast.makeText(PackageInfoActivity.this,"保存失败",Toast.LENGTH_SHORT).show();
							}
						}
					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(PackageInfoActivity.this,"盘点失败!",Toast.LENGTH_SHORT).show();
					}
    				break;
    			case 2:
    				break;
    			default:
    				break;
    			}
            }

    };
}
