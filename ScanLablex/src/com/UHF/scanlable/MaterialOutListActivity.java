package com.UHF.scanlable;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.UHF.R;
import com.UHF.model.AcceptanceAdapter;
import com.UHF.turntable.CircleActivity;
import com.UHF.util.CheckDataFromServer;

public class MaterialOutListActivity extends Activity implements OnClickListener{

	Button refresh;
	Button incomingMButton;
	/**
	 * 单据号/状态
	 */
	private LinkedHashMap<Object,Object> listData=new LinkedHashMap<Object,Object>();
	
	private Map allData=new HashMap();
	private AcceptanceAdapter myAdapter;
	private ListView listView;
	private Intent intent;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.material_out_list);
		refresh = (Button)findViewById(R.id.mo_refresh1);
		refresh.setOnClickListener(this);
		listView = (ListView)findViewById(R.id.materialOutList);//
		intent = new Intent(this,MaterialOutActivity.class);
		listView.setOnItemClickListener(new OnItemClickListener(){  
			  
            @Override  
            public void onItemClick(AdapterView<?> parent, View view,  
                    int position, long id) {  
        		String documentNo = myAdapter.getmList().get(position);
        		Map m=(Map) allData.get(documentNo);
        		intent.putExtra("extendOrderCode", m.get("extendOrderCode").toString());
        		intent.putExtra("issueCode", m.get("issueCode").toString());
        		intent.putExtra("orderState", m.get("orderState").toString());
        		intent.putExtra("extendingMaterialId", m.get("extendingMaterialId").toString());
//        		intent.putExtra(MainActivity.EXTRA_EPC, myAdapter.getmList().get(position));
        		startActivity(intent);
        		finish();
            }  
        });  
		refreshList();
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==refresh.getId()){
			myAdapter = null;
			listData.clear();
			allData.clear();
			refreshList();
		}
	}
	public boolean refreshList(){
		CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
		Map m=new HashMap();
		m.put("user_id", UfhData.getEmployBean().getId());
		checkDataFromServer.setOperType("13");//成品入库待复核单据
		checkDataFromServer.setData(m);
		checkDataFromServer.setIp(UfhData.getIP());
		checkDataFromServer.setmHandler(handler);
		checkDataFromServer.setWhat(0);
		checkDataFromServer.checkData();
		return true;
	}
	
	
	

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, CircleActivity.class); 
        startActivity(intent);
        finish();
	}



	private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        		super.handleMessage(msg);
                switch (msg.what) {
    			case 0:
    				try {
    					String s= (String) msg.obj;
    					if(s==null||"-1".equals(s)){
    						Toast.makeText(MaterialOutListActivity.this,"无数据!",Toast.LENGTH_SHORT).show();
    					}else{
    						JSONArray objList;
    						try {
    		    				JSONObject ss= new JSONObject(s);
    		    				String operType=ss.getString("operType");
    							objList = new JSONArray(ss.getString("list"));
    							String types="";
    							for (int i = 0; i< objList.length(); i++) {
    				                //循环遍历，依次取出JSONObject对象
    				                //用getInt和getString方法取出对应键值
    								Map<Object,Object> m=new HashMap<Object,Object>();
    				                JSONObject obj = objList.getJSONObject(i);
    				                /*acceptanceData.put(checkHex(Long.toHexString(Long.parseLong(obj.get("epcId").toString())).toUpperCase(),"0001"),obj.get("packageInventoryNum"));//epc,流水号
    				                String backGroundColor=obj.get("backGroundColor").equals("0")?"已复核":"未复核";
    				                acceptanceCheckData.put(obj.get("packageInventoryNum").toString(),backGroundColor);//epc,流水号*/
    				                types=obj.getString("orderState");
    				                if("0".equals(types)){
    				                	types="待领料";
    				                }else if("1".equals(types)){
    				                	types="领料中";
    				                }else if("3".equals(types)){
    				                	types="待发料";
    				                }
    				                try {
    				    				m.put("extendOrderCode", obj.getString("extendOrderCode"));//出库单据编号
    				    				m.put("issueCode", obj.getString("issueCode"));//领料单编号
    				    				m.put("orderState", obj.getString("orderState"));//状态
    				    				m.put("extendingMaterialId", obj.getString("extendingMaterialId"));//id
    					                listData.put(obj.get("extendOrderCode"), types);
    					                allData.put(obj.get("extendOrderCode"), m);
    								} catch (Exception e) {
    									// TODO: handle exception
    								}
    				             }
    						} catch (JSONException e) {
    							// TODO Auto-generated catch block
    							e.printStackTrace();
    						}
    						if(myAdapter == null){
    							myAdapter = new AcceptanceAdapter(MaterialOutListActivity.this, listData);
    							listView.setAdapter(myAdapter);
    						}else{
    							myAdapter.setData(listData) ;
    						}
    						myAdapter.notifyDataSetChanged();
    					}
					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(MaterialOutListActivity.this,"无数据!",Toast.LENGTH_SHORT).show();
    				}
    				break;
    			case 1:
    				break;
    			case 2:
    				break;
    			default:
    				break;
    			}
            }

    };
}
