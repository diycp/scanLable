package com.UHF.scanlable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.UHF.R;
import com.UHF.model.AcceptanceAdapter;
import com.UHF.model.CheckBoxAdapter;
import com.UHF.turntable.CircleActivity;
import com.UHF.turntable.ScanActivity;
import com.UHF.util.CheckDataFromServer;


public class ChangeBandingDetilActivity extends ScanActivity implements OnClickListener{
	//标题
	private TextView mo_title;
	/**
	 * 0:入托，1托盘出库2物料出库
	 */
	private String type="0";
	/**
	 * 按钮类型//1物料2托盘3库位
	 */
	private String lastBt="-1";
	/**
	 * 按钮类型//1物料2托盘3库位
	 */
	private String thisBt="0";
	
	
	private ListView listView;
	private AcceptanceAdapter myAdapter;
	private CheckBoxAdapter checkAdapter;
	private Map<String,Integer> data=new HashMap<String, Integer>();
	private Map<String,Integer> oldData=new HashMap<String, Integer>();
	//个数
	private TextView allCounts;
	//个数
	private Integer counts= 0;
	/**
	 * 新扫描确认的数据
	 */
	private List<String> materialAddData=new ArrayList<String>();
	/**
	 * 删除的数据
	 */
	private  List<String> materialDelData=new ArrayList<String>();
	private Handler mHandler;
//	private String documentNo;
//	private Intent intent;
//	private String goodsName;
//	private String goodsCode;
//	private String demandCount;
//	private String assignCount;
	private String outboundDemandId;
	private Button mo_scanMaterial_bt;//扫物料
	private Button mo_checkIt_bt;//确定
	private Button deleteAssignPackage;//删除
	private String extendOrderCode;
	private String issueCode;
	private String orderState;
	private String extendingMaterialId;

	//标题
	private TextView cp_title;
	/**
	 * 0:物料，1成品
	 */
	String change_type="0";
	/**
	 * 单据号/状态
	 */
	private LinkedHashMap<Object,Object> listData=new LinkedHashMap<Object,Object>();
	private LinkedHashMap<Object,Object> trayList=new LinkedHashMap<Object,Object>();
	private LinkedHashMap<Object,Object> trayListx=new LinkedHashMap<Object,Object>();
	private LinkedHashMap<Object,Object> wareHouseList=new LinkedHashMap<Object,Object>();
	private LinkedHashMap<Object,Object> wareHouseListx=new LinkedHashMap<Object,Object>();
	private List<String> trayLists=new ArrayList<String>();
	private List<String> wareHouseLists=new ArrayList<String>();
	String viewId="";
	String documentNo="";
	Button confirmBanding;
	Button cp_scanStock_btx;
	Button cp_scanTray_btx;
	Button incomingMButton;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.banding_position);
		change_type=getIntent().getStringExtra("change_type");
		viewId=getIntent().getStringExtra("viewId");
		documentNo=getIntent().getStringExtra("documentNo");
		confirmBanding = (Button)findViewById(R.id.confirmBanding);
		confirmBanding.setOnClickListener(this);
		cp_scanTray_btx = (Button)findViewById(R.id.cp_scanTray_btx);
		cp_scanTray_btx.setOnClickListener(this);
		cp_scanStock_btx = (Button)findViewById(R.id.cp_scanStock_btx);
		cp_scanStock_btx.setOnClickListener(this);
		listView = (ListView)findViewById(R.id.check_position_listxx);//
		//标题
		cp_title = (TextView)findViewById(R.id.cp_titlexx);//
		if("0".equals(change_type)){
			cp_title.setText(R.string.changPositionTitle2);
		}else if("1".equals(change_type)){
			cp_title.setText(R.string.changPositionTitle4);
		}
		cp_title.setText(cp_title.getText()+" 单据号:"+documentNo);
		mHandler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
	    		super.handleMessage(msg);
				switch (msg.what) {
				case 0:
					if(isCanceled) return;
					if("-1".equals(lastBt)){//非扫码状态
						return;
					}
					data = UfhData.scanResult6c;
					if(data==null||data.isEmpty()){
						break;
					}
					List checkList=checkMapData(oldData,data);
					if(checkList.size()>0){
						CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
						String s=StringUtils.join(checkList.toArray(), ",");
						Map m=new HashMap();
						m.put("scanCode", s);
						m.put("user_id", UfhData.getEmployBean().getId());
						m.put("type", type);//解绑扫物料
//						if(("1".equals(lastBt)||"-1".equals(lastBt))&&"0".equals(change_type)){
//							checkDataFromServer.setOperType("1");
//						}else if(("1".equals(lastBt)||"-1".equals(lastBt))&&"1".equals(change_type)){//扫成品
//							checkDataFromServer.setOperType("30");
//						}else 
						if("2".equals(lastBt)){//扫托盘
							checkDataFromServer.setOperType("2");
						}else if("3".equals(lastBt)){//扫库位
							checkDataFromServer.setOperType("3");
						}
						checkDataFromServer.setData(m);
						checkDataFromServer.setIp(UfhData.getIP());
						checkDataFromServer.setmHandler(handler);
						checkDataFromServer.setWhat(0);
						if(checkDataFromServer.checkData()){
							oldData=new HashMap<String, Integer>(data);
						}
					}
					break;
				case 1:
					break;
				case 2:
					break;
				default:
					break;
				}
			}
		};
		refreshList();
		
	}
	
	public boolean refreshList(){
		materialAddData.clear();
		materialDelData.clear();

		listData.clear();
		trayLists.clear();
		trayListx.clear();
		trayList.clear();
		wareHouseListx.clear();
		wareHouseLists.clear();
		wareHouseList.clear();
		
		data.clear();
		oldData.clear();
		checkAdapter= null;
		return true;
	}

	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
		Map m=new HashMap();
		switch (v.getId()) {
//		case R.id.mo_scanMaterial_bt://扫物料
//			if("1".equals(lastBt)){
//				lastBt="-1";
//				if("0".equals(change_type)){
//					mo_scanMaterial_bt.setText(R.string.scan);
//				}else{
//					mo_scanMaterial_bt.setText(R.string.scanProduct);
//				}
//				closeRFIDScan();
//				data.clear();
//				oldData.clear();
//			}else if("-1".equals(lastBt)){
//				lastBt="1";//查物料
//				mo_scanMaterial_bt.setText(R.string.stop);
//				if("0".equals(change_type)){
//					openRFIDScan(mHandler,UfhData.scanResult6c,1,"0001");
//				}else{
//					openRFIDScan(mHandler,UfhData.scanResult6c,1,"0004");
//				}
//			}else{
//				Toast.makeText(ChangeBandingDetilActivity.this,"请先结束当前任务!",Toast.LENGTH_SHORT).show();
//			}
		case R.id.mo_refresh://刷新
			refreshList();
			break;
		case R.id.cp_scanTray_btx://扫托盘
			if("2".equals(lastBt)){
//				trayLists.clear();
//				trayList.clear();
//				wareHouseList.clear();
				lastBt="-1";
				cp_scanTray_btx.setText(R.string.sacnTray);
				closeRFIDScan();
				data.clear();
				oldData.clear();
			}else if("-1".equals(lastBt)){
				for(Object key:wareHouseList.keySet()){
					wareHouseLists.add(wareHouseListx.get(key).toString());
				}
				thisBt="2";
				listData.clear();
				checkAdapter= null;
				lastBt="2";//查物料
				cp_scanTray_btx.setText(R.string.stop);
				openRFIDScan(mHandler,UfhData.scanResult6c,1,"0002");
			}else{
				Toast.makeText(ChangeBandingDetilActivity.this,"请先结束当前任务!",Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.cp_scanStock_btx://扫库位
			if("3".equals(lastBt)){
//				trayList.clear();
				lastBt="-1";
				cp_scanStock_btx.setText(R.string.scanStock);
				closeRFIDScan();
				data.clear();
				oldData.clear();
			}else if("-1".equals(lastBt)){
				for(Object key:trayList.keySet()){
					trayLists.add(trayListx.get(key).toString());
				}
				thisBt="3";
				listData.clear();
				checkAdapter= null;
				lastBt="3";//查物料
				cp_scanStock_btx.setText(R.string.stop);
				openRFIDScan(mHandler,UfhData.scanResult6c,1,"0003");
			}else{
				Toast.makeText(ChangeBandingDetilActivity.this,"请先结束当前任务!",Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.confirmBanding://确定
			if("-1".equals(lastBt)){
				m.put("operateType", "3");
				m.put("type", "1");//解绑/绑定
				m.put("user_id", UfhData.getEmployBean().getId());
				m.put("change_type", change_type);
				m.put("viewId", viewId);
				if("2".equals(thisBt)){
					trayLists=checkAdapter.getChecked();
				}else{
					wareHouseLists=checkAdapter.getChecked();
				}
				m.put("trayEpcId", StringUtils.join(trayLists.toArray(), ","));
				m.put("wareHouseEpcId", StringUtils.join(wareHouseLists.toArray(), ","));
				if(wareHouseLists.size()>0){
					checkDataFromServer.setOperType("31");//换库位
					checkDataFromServer.setData(m);
					checkDataFromServer.setIp(UfhData.getIP());
					checkDataFromServer.setmHandler(handler);
					checkDataFromServer.setWhat(1);
					if(checkDataFromServer.checkData()){
						oldData=new HashMap<String, Integer>(data);
					}
				}else{
					Toast.makeText(ChangeBandingDetilActivity.this,"必须扫描库位!",Toast.LENGTH_SHORT).show();
				}	
			}else{
				Toast.makeText(ChangeBandingDetilActivity.this,"请先结束当前任务!",Toast.LENGTH_SHORT).show();
			}
			break;
		default:
			break;
		}
		return;
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		return super.onKeyDown(keyCode, event);
	}
	@Override
	protected void showScanCode() {
		// TODO Auto-generated method stub
		super.showScanCode();
//		Toast.makeText(ProductOutActivity.this,getBarcodeStr(),Toast.LENGTH_SHORT).show();
		data.put(getBarcodeStr(), 1);
		List checkList=checkMapData(oldData,data);
		if(checkList.size()>0){
			CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
			String s=StringUtils.join(checkList.toArray(), ",");
			Map m=new HashMap();
			m.put("scanCode", s);
			m.put("user_id", UfhData.getEmployBean().getId());
			m.put("type", type);//解绑扫物料
//			if(("1".equals(lastBt)||"-1".equals(lastBt))&&"0".equals(change_type)){
//				checkDataFromServer.setOperType("1");
//			}else if(("1".equals(lastBt)||"-1".equals(lastBt))&&"1".equals(change_type)){//扫成品
//				checkDataFromServer.setOperType("30");
//			}else 
			if("2".equals(lastBt)){//扫托盘
				checkDataFromServer.setOperType("2");
			}else if("3".equals(lastBt)){//扫库位
				checkDataFromServer.setOperType("3");
			}
			checkDataFromServer.setData(m);
			checkDataFromServer.setIp(UfhData.getIP());
			checkDataFromServer.setmHandler(handler);
			checkDataFromServer.setWhat(0);
			if(checkDataFromServer.checkData()){
				oldData=new HashMap<String, Integer>(data);
			}
		}
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, MaterialOutListActivity.class);  
        startActivity(intent);
        finish();
	}
	private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        		super.handleMessage(msg);
        		try {
        			String s= (String) msg.obj;
                    switch (msg.what) {
                    case 0:
        				try {
        					if(s==null||"-1".equals(s)){
        						Toast.makeText(ChangeBandingDetilActivity.this,"扫码错误!",Toast.LENGTH_SHORT).show();
        					}else{
        						JSONArray objList;
        						try {
        		    				JSONObject ss= new JSONObject(s);
        		    				String operType=ss.getString("operType");
        							objList = new JSONArray(ss.getString("list"));
        							String typeName="";
        							String types="";
        							if("1".equals(operType)){
        								typeName="packageInventoryNum";
        								types="物料";
        			                }else if("2".equals(operType)){
        								typeName="code";
        								types="托盘";
        			                }else if("3".equals(operType)){
        								typeName="positionCode";
        								types="库位";
        			                }else if("30".equals(operType)){
        								typeName="packageInventoryNum";
        								types="成品";
        			                }
        							for (int i = 0; i< objList.length(); i++) {
        				                //循环遍历，依次取出JSONObject对象
        				                //用getInt和getString方法取出对应键值
        				                JSONObject obj = objList.getJSONObject(i);
        				                /*acceptanceData.put(checkHex(Long.toHexString(Long.parseLong(obj.get("scanCode").toString())).toUpperCase(),"0001"),obj.get("packageInventoryNum"));//epc,流水号
        				                String backGroundColor=obj.get("backGroundColor").equals("0")?"已复核":"未复核";
        				                acceptanceCheckData.put(obj.get("packageInventoryNum").toString(),backGroundColor);//epc,流水号*/
        				                if("2".equals(operType)){
        				                	trayList.put(obj.get(typeName).toString(), types);
        				                	trayListx.put(obj.getString(typeName), obj.getString("scanCode"));
        				                	listData.putAll(trayList);
        				                }else if("3".equals(operType)){
        				                	wareHouseList.put(obj.get(typeName).toString(), types);
        				                	wareHouseListx.put(obj.getString(typeName), obj.getString("scanCode"));
        				                	listData.putAll(wareHouseList);
        				                }
        				             }
//    								if("1".equals(operType)||"30".equals(operType)){
//    									if(myAdapter == null){
//    										myAdapter = new AcceptanceAdapter(ChangeBandingDetilActivity.this, listData);
//    										listView.setAdapter(myAdapter);
//    									}else{
//    										myAdapter.setData(listData) ;
//    									}
//    									myAdapter.notifyDataSetChanged();
//    								  }else{
    									if(checkAdapter == null){
    										checkAdapter = new CheckBoxAdapter(ChangeBandingDetilActivity.this, listData);
    										listView.setAdapter(checkAdapter);
    									}else{
    										checkAdapter.setData(listData) ;
    									}
    									checkAdapter.notifyDataSetChanged();
//    								}
        						} catch (JSONException e) {
        							// TODO Auto-generated catch block
        							e.printStackTrace();
        						}
        						
        			            
        					}
    					} catch (Exception e) {
    						// TODO: handle exception
    						Toast.makeText(ChangeBandingDetilActivity.this,"扫码错误!",Toast.LENGTH_SHORT).show();
        				}
        				break;
                    case 1:
        				try {
        					if(s==null||"-1".equals(s)){
        						Toast.makeText(ChangeBandingDetilActivity.this,"扫码错误!",Toast.LENGTH_SHORT).show();
        					}else{
        						String re="操作成功";
    							JSONObject ss= new JSONObject(s);
    							String operType=ss.getString("operType");
    							String change_typex=ss.getString("change_type");
    							String type=ss.getString("type");
    							String result=ss.getString("result");
    							if("0".equals(result)){
    								if("0".equals(type)&&"0".equals(change_typex)){
    									Toast.makeText(ChangeBandingDetilActivity.this,R.string.changPositionTitle1+"完成",Toast.LENGTH_SHORT).show();
    								}else if("1".equals(type)&&"0".equals(change_typex)){
    									Toast.makeText(ChangeBandingDetilActivity.this,R.string.changPositionTitle2+"完成",Toast.LENGTH_SHORT).show();
    								}else if("0".equals(type)&&"1".equals(change_typex)){
    									Toast.makeText(ChangeBandingDetilActivity.this,R.string.changPositionTitle3+"完成",Toast.LENGTH_SHORT).show();
    								}else if("1".equals(type)&&"1".equals(change_typex)){
    									Toast.makeText(ChangeBandingDetilActivity.this,R.string.changPositionTitle4+"完成",Toast.LENGTH_SHORT).show();
    								}
    								Intent intent = new Intent(ChangeBandingDetilActivity.this, CircleActivity.class);  
    						        startActivity(intent); 
    								finish();
    							}else if("15".equals(result)){
    								Toast.makeText(ChangeBandingDetilActivity.this,"包装占用！",Toast.LENGTH_SHORT).show();
    							}else if("-1".equals(result)){
    								Toast.makeText(ChangeBandingDetilActivity.this,"操作错误！",Toast.LENGTH_SHORT).show();
    							}else if("-2".equals(result)){
    								Toast.makeText(ChangeBandingDetilActivity.this,"托盘/库位有误！",Toast.LENGTH_SHORT).show();
    							}else if("4".equals(result)){
    								Toast.makeText(ChangeBandingDetilActivity.this,"该库位已冻结！",Toast.LENGTH_SHORT).show();
    							}else if("7".equals(result)){
    								Toast.makeText(ChangeBandingDetilActivity.this,"托盘未入库！",Toast.LENGTH_SHORT).show();
    							}else {
    								Toast.makeText(ChangeBandingDetilActivity.this,"操作错误!",Toast.LENGTH_SHORT).show();
    							}
    						}
    					} catch (Exception e) {
    						// TODO: handle exception
    						Toast.makeText(ChangeBandingDetilActivity.this,"操作错误!",Toast.LENGTH_SHORT).show();
    					}
        				break;
        			default:
        				break;
        			}
				} catch (Exception e) {
					// TODO: handle exception
					Toast.makeText(ChangeBandingDetilActivity.this,"操作错误!",Toast.LENGTH_SHORT).show();
				}
            }

    };
}
