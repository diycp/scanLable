package com.UHF.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.os.Handler;
import android.os.Message;

import com.UHF.scanlable.HttpUtil;

/**
 * 连接服务器的接口 mHandler返回回调消息队列，operType对应服务端接口，what回调消息类中的what值
 * @author major
 *
 */
public class CheckDataFromServer {
	private List<NameValuePair> nameValuePairs=new ArrayList<NameValuePair>(); 
	private String ip;
	private Handler mHandler;
	private String operType;
	private Integer what;


	public boolean checkData(){
		if(ip==null){
			return false;
		}
		new Thread(new Runnable(){
		public void run(){
			HttpUtil h=new HttpUtil();
    		h.setUrl(ip);
	        nameValuePairs.add(new BasicNameValuePair("operType", operType));//根据批号查询包装
//	        nameValuePairs.add(new BasicNameValuePair("acceptanceCode", acceptance_No));//根据单据号（物料复核）
        	Object s=h.getHttpResult(nameValuePairs);
			Message m=new Message();
			m.obj=s;
			m.what=what;
			mHandler.sendMessage(m);
			}
		}).start();
		return true;
	}



	/**
	 * @param data 放入需要传的参数
	 */
	public void setData(Map data) {
		
		Set<String> set = data.keySet();
		Iterator<String> it = set.iterator();
		while(it.hasNext())
		{
		    String key = it.next();
		    nameValuePairs.add(new BasicNameValuePair(key,  data.get(key).toString()));
		}
	}

	public Handler getmHandler() {
		return mHandler;
	}


	public void setmHandler(Handler mHandler) {
		this.mHandler = mHandler;
	}


	public String getOperType() {
		return operType;
	}


	public void setOperType(String operType) {
		this.operType = operType;
	}



	public String getIp() {
		return ip;
	}



	public void setIp(String ip) {
		this.ip = ip;
	}



	public Integer getWhat() {
		return what;
	}



	public void setWhat(Integer what) {
		this.what = what;
	}
	
}
